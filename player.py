from termcolor import colored

from dealer import Dealer
from hand import Hand
from strategy import Strategy


class Player:
    type = ''
    name = ''
    wager = None
    bankroll = None
    hand = None
    strategy = None
    move_translate = {
        'h': 'Hit',
        's': 'Stand',
        'sp': 'Split',
        'dd': 'Double down'
    }
    stats = None

    def __init__(self, name):
        self.hand = Hand()
        self.name = name
        self.bankroll = 10000
        self.strategy = Strategy()
        self.stats = {
            "Blackjack": 0,
            "Win": 0,
            "Lose": 0,
            "Push": 0,
            "Bust": 0,
            "Rounds": 0,
            "Hit": 0,
            "Stand": 0,
            "Double down": 0,
            "Decisions": 0,
        }

    '''
    Make a wager
    '''

    def make_wager(self):
        player_wager = False
        while player_wager is False:
            wager = Dealer.input(self.name + ", how much would you like to wager?")
            if wager.isnumeric() is False:
                player_wager = False
                Dealer.say("Invalid. Your bet must be an integer greater than 0")
            else:
                if int(wager) <= 0:
                    player_wager = False
                    Dealer.say("Bet is too low. Bets must be greater than 0")
                else:
                    player_wager = True
                    if int(wager) > self.bankroll:
                        # Prevent wages greater than bankroll
                        wager = self.bankroll
                    self.wager = int(wager)
                    player = colored("\n" + self.name + ': ', 'magenta')
                    colored_msg = colored('$' + str(self.wager))
                    print(player + colored_msg)

    '''
    View cards in player's hand
    '''

    def peek_hand(self):
        self.hand.get_hand()

    '''
    Get the player decision
    '''

    def player_decision(self, dealers_card):
        recommended = self.recommend_move(dealers_card)

        if len(self.hand.cards) < 3:
            valid_moves = ['h', 's', 'sp', 'dd', 'r']
            # tip = "(h = hit, s = stand, sp = split, dd = double down, r = recommended)"
            tip = "(h = hit, s = stand, dd = double down, r = recommended [%s])" % self.move_translate.get(recommended)
        else:
            valid_moves = ['h', 's']
            tip = "(h = hit, s = stand, r = recommended [%s])" % self.move_translate.get(recommended)

        move = False
        while move is False:
            move = input("\n" + self.name + "'s turn: " + tip + ' ')
            if move not in valid_moves:
                Dealer.say("Not a valid move")
                move = False
        if move == 'r':
            move = recommended
        player = colored("\n" + self.name + ': ', 'magenta')
        colored_msg = colored(self.move_translate.get(move) + ' please.', 'white')
        print(player + colored_msg)
        return move

    def recommend_move(self, dealers_card):
        move = ''
        move_arr = []
        hand_hard = self.hand.get_value()
        hand_soft = self.hand.get_value_soft()
        dealer_card_value = dealers_card.get_value()
        index = dealer_card_value - 2

        if hand_hard == hand_soft:
            if hand_hard < 22:
                move_arr = self.strategy.hard_strategy.get(hand_hard)
            else:
                move = 's'
        else:
            if 12 < hand_soft <= 21 < hand_hard:
                move_arr = self.strategy.soft_strategy.get(hand_soft)
            else:
                if 12 < hand_hard < 22:
                    move_arr = self.strategy.soft_strategy.get(hand_hard)
                else:
                    # TODO: when pairs strategy is enabled, this should be removed
                    if hand_hard < 22:
                        move_arr = self.strategy.hard_strategy.get(hand_hard)
                    else:
                        move = 's'

        if move == '':
            move = move_arr[index]

        '''
        Move correction, fix later
        '''
        if move == 'dh':
            if len(self.hand.cards) == 2:
                move = 'dd'
            else:
                move = 'h'
        elif move == 'ds':
            if len(self.hand.cards) == 2:
                move = 'dd'
            else:
                move = 's'
        elif move == 'rh':
            move = 'h'

        # Stats
        if move == 'h':
            self.stats['Hit'] += 1
        elif move == 's':
            self.stats['Stand'] += 1
        elif move == 'dd':
            self.stats['Double down'] += 1

        self.stats['Decisions'] += 1

        return move

# END
