import sys
import json
import time
from random import randint

import names
from termcolor import colored

from bot import Bot
from dealer import Dealer
from player import Player


class Game:
    round = 1
    dealer = None
    players = []
    max_players = 16
    num_players = 0
    state = "RUN"
    deck_count = 5
    turn_time = 0.35

    def __init__(self):
        self.init_players()
        self.loop()

    '''
    Game loop
    '''

    def loop(self):
        while self.state == "RUN":
            self.play_round()
            '''
            END GAME STATES
            '''
            # If there are no players left, game over
            if len(self.players) == 0:
                self.state = "GAME_OVER"  # GAME_OVER
                Game.say("There are no players remaining.")
                Game.say("The house ALWAYS wins! Mwahahaha!")
                Game.msg("GAME OVER\n\n")
            # If the dealer's bank is empty, all players win
            if self.dealer.bank <= 0:
                self.state = "WIN"
                for player in self.players:
                    self.round_stats(player)
                Game.say("Uhhh... We're out of cash.. Let's get the hell out of here.")
                Game.msg("Congratulations! The players have beat the house!\n\n")
                Game.msg("GAME OVER\n\n")

    '''
    Play the round
    '''

    def play_round(self):
        self.start_round()
        self.place_bets()
        self.deal_cards()
        self.player_turns()
        self.dealer_turn()
        self.determine_payout()
        self.end_round()

    '''
    Round start
    '''

    def start_round(self):
        round_start = "\n------------------------------ ROUND " + str(self.round) + " ------------------------------"
        round_format = colored(round_start, 'yellow')
        Game.msg(round_format)

    '''
    Get bets from humans and bots    
    '''

    def place_bets(self):
        Dealer.say("Place your bets")
        self.wager_bets()

    '''
    Deal the first cards of the round
    '''

    def deal_cards(self):
        # Deal first card to players
        self.player_deal()
        print("\n")
        for player in self.players:
            print(player.name)
            player.hand.get_hand()
            time.sleep(self.turn_time * 2)
        # Dealer's card
        self.dealer_deal()
        print("\n\nDealer's card")
        self.dealer.peek_hand()
        time.sleep(self.turn_time * 2)
        # Deal second card to players
        self.player_deal()
        print("\n")
        for player in self.players:
            print(player.name)
            player.hand.get_hand()
            time.sleep(self.turn_time * 2)

    '''
    Deal 1 card to each player
    '''

    def player_deal(self):
        for player in self.players:
            card = self.dealer.draw_card()
            player.hand.cards.append(card)

    '''
    Deal 1 card to the dealer
    '''

    def dealer_deal(self):
        dealer_card = self.dealer.draw_card()
        self.dealer.hand.cards.append(dealer_card)

    '''
    Collect each players bets
    '''

    def wager_bets(self):
        for player in self.players:
            player.make_wager()

    '''
    Players' turns
    '''

    def player_turns(self):
        # Show the dealer's card
        # Get each player's action
        for player in self.players:
            player_start = "\n---------- " + player.name + " ----------"
            player_format = colored(player_start, 'yellow')
            Game.msg(player_format)
            action = None
            double_down = False
            dealer_hard_value = self.dealer.hand.get_value()
            dealer_soft_value = self.dealer.hand.get_value_soft()

            if dealer_hard_value < 22 and dealer_hard_value != dealer_soft_value:
                term = str(dealer_soft_value) + ' or ' + str(dealer_hard_value)
            else:
                term = str(dealer_soft_value)

            Game.msg("Dealer's card (%s):" % term)
            self.dealer.peek_hand()
            while action != 's':
                hand_count = len(player.hand.cards)
                hard_value = player.hand.get_value()
                soft_value = player.hand.get_value_soft()
                player_blackjack = hand_count < 3 and hard_value == 21 and soft_value == 11

                if hard_value < 22 and hard_value != soft_value and player_blackjack is False:
                    term = str(soft_value) + ' or ' + str(hard_value)
                else:
                    term = str(soft_value)

                if double_down is True:
                    Game.msg(player.name + "'s hand (%s):" % term)
                    player.peek_hand()
                    action = 's'
                elif player_blackjack is True:
                    Game.msg(player.name + "'s hand (Blackjack):")
                    player.peek_hand()
                    Dealer.say("Blackjack! Congratulations!")
                    action = 's'
                else:
                    Game.msg(player.name + "'s hand (%s):" % term)
                    player.peek_hand()
                    # Check bust
                    if hard_value > 21 and soft_value > 21:
                        Dealer.say("Bust! Sorry about that!")
                        action = 's'
                    else:
                        dealers_card = self.dealer.hand.cards[0]
                        action = player.player_decision(dealers_card)
                        self.process_action_player(player, action)
                        if action == 'dd':
                            # Double down only receives 1 card.
                            double_down = True
                time.sleep(self.turn_time)

    '''
    Player's action
    '''

    def process_action_player(self, player, action):
        if action == 'h':
            card = self.dealer.draw_card()
            player.hand.cards.append(card)
        elif action == 's':
            pass
        elif action == 'sp':
            #  NOT YET IMPLEMENTED
            pass
        elif action == 'dd':
            player.wager *= 2
            card = self.dealer.draw_card()
            player.hand.cards.append(card)
            pass
        else:
            Dealer.say("Unknown action!")

    '''
    Dealer's action
    '''

    def process_action_dealer(self, action):
        if action == 'h':
            card = self.dealer.draw_card()
            self.dealer.hand.cards.append(card)
        elif action == 's':
            pass

    '''
    Dealer's turn
    '''

    def dealer_turn(self):
        action = None
        # In this loop, we will determine if the dealer's turn is needed (ie, every other player lost or bust -- so
        # the dealer's turn is not necessary)
        players_bust = 0
        for player in self.players:
            hard_value = player.hand.get_value()
            soft_value = player.hand.get_value_soft()
            if hard_value > 21 and hard_value == soft_value:
                players_bust += 1
        # Decide if turn is necessary
        if int(players_bust) == int(self.num_players):
            action = 's'
        else:
            dealer_start = "\n---------- Dealer's turn! ----------"
            dealer_format = colored(dealer_start, 'yellow')
            Game.msg(dealer_format)

        while action != 's':
            hand_count = len(self.dealer.hand.cards)
            hard_value = self.dealer.hand.get_value()
            soft_value = self.dealer.hand.get_value_soft()
            dealer_blackjack = hand_count < 3 and hard_value == 21 and soft_value == 11
            if hard_value < 22 and hard_value != soft_value:
                term = str(soft_value) + ' or ' + str(hard_value)
            else:
                term = str(soft_value)
            Game.msg("Dealer's hand (%s):" % term)
            self.dealer.peek_hand()
            if dealer_blackjack:
                Dealer.say("Blackjack!")
                action = 's'
            elif hard_value > 21 and soft_value > 21:
                Dealer.say("Oops! Dealer Busts!")
                action = 's'
            else:
                action = self.dealer.make_decision()
                self.process_action_dealer(action)
            time.sleep(self.turn_time)

    '''
    Determine payouts / losses for each player
    '''

    def determine_payout(self):
        dealer_hand = self.dealer.hand.get_value()
        dealer_bust = dealer_hand > 21
        output_msg = ""

        for player in self.players:
            dealer_hand_count = len(self.dealer.hand.cards)
            dealer_hard_value = self.dealer.hand.get_value()
            dealer_soft_value = self.dealer.hand.get_value_soft()
            dealer_blackjack = dealer_hand_count < 3 and dealer_hard_value == 21 and dealer_soft_value == 11

            hand_count = len(player.hand.cards)
            player_hand_hard = player.hand.get_value()
            player_hand_soft = player.hand.get_value_soft()
            player_blackjack = hand_count < 3 and player_hand_hard == 21 and player_hand_soft == 11
            player_bust = player_hand_hard > 21
            winnings = ''

            # Get result
            if player_bust is True:
                result = 'LOSE'
                player.stats['Bust'] += 1
            elif player_blackjack is True and dealer_blackjack is False:
                result = 'BLACKJACK'
            else:
                if dealer_bust is True:
                    if player_hand_hard < 22:
                        result = 'WIN'
                    else:
                        result = 'LOSE'
                else:
                    if player_hand_hard == dealer_hand:
                        result = 'PUSH'
                    elif player_hand_hard > dealer_hand:
                        result = 'WIN'
                    else:
                        result = 'LOSE'

            # inc / dec bankroll
            if result == 'PUSH':
                winnings += '  $0'
                player.stats['Push'] += 1
            elif result == 'LOSE':
                # We don't want negative values, the player would owe money..
                if player.bankroll - int(player.wager) < 0:
                    player.wager = player.bankroll
                player.bankroll -= int(player.wager)
                self.dealer.bank += int(player.wager)
                winnings += '- $' + str(player.wager)
                winnings = colored(winnings, 'red')
                player.stats['Lose'] += 1
            elif result == 'WIN':
                player.bankroll += int(player.wager)
                self.dealer.bank -= int(player.wager)
                winnings += '+ $' + str(player.wager)
                winnings = colored(winnings, 'green')
                player.stats['Win'] += 1
            elif result == 'BLACKJACK':
                player.wager = round(int(player.wager) * 1.5)
                player.bankroll += int(player.wager)
                self.dealer.bank -= int(player.wager)
                winnings += '+ $' + str(player.wager)
                winnings = colored(winnings, 'green')
                player.stats['Blackjack'] += 1
                player.stats['Win'] += 1

            if result == 'WIN' or result == 'BLACKJACK':
                symbol = '✓'
                symbol = colored(symbol, 'green')
            elif result == 'LOSE':
                symbol = '✕'
                symbol = colored(symbol, 'red')
            else:
                symbol = ' '
            space = ' '
            _player = colored(player.name, 'magenta')
            result_text = symbol + space.ljust(3) + winnings + "\t → "
            output_msg += "\n" + result_text + ' $' + str(player.bankroll).ljust(5) + "\t →  " + _player

        round_results = "\n---------- ROUND " + str(self.round) + " RESULTS ----------"
        round_format = colored(round_results, 'yellow')
        Game.msg(round_format)
        Game.msg(output_msg)
        Game.msg("House Bank: $" + str(self.dealer.bank))
        Game.msg("\n")
        time.sleep(self.turn_time * 5)

    '''
    Display player stats
    '''

    def round_stats(self, player):
        Game.say(player.name + " stats:")
        if player.stats.get('Win') > 0:
            blackjacks = (player.stats.get('Blackjack') / player.stats.get('Win')) * 100
        else:
            blackjacks = 0
        if player.stats.get('Lose') > 0:
            busts = (player.stats.get('Bust') / player.stats.get('Lose')) * 100
        else:
            busts = 0
        wins = (player.stats.get('Win') / player.stats.get('Rounds')) * 100
        loses = (player.stats.get('Lose') / player.stats.get('Rounds')) * 100
        pushes = (player.stats.get('Push') / player.stats.get('Rounds')) * 100
        hits = (player.stats.get('Hit') / player.stats.get('Decisions')) * 100
        stands = (player.stats.get('Stand') / player.stats.get('Decisions')) * 100
        doubles = (player.stats.get('Double down') / player.stats.get('Decisions')) * 100
        stats = {
            "Round Stats": {
                "Win": {
                    "Count": player.stats.get('Win'),
                    "Percent": '{0:.2f}'.format(wins) + '%',
                    "Blackjack": {
                        "Count": player.stats.get('Blackjack'),
                        "Percent": '{0:.2f}'.format(blackjacks) + '%',
                    },
                },
                "Lose": {
                    "Count": player.stats.get('Lose'),
                    "Percent": '{0:.2f}'.format(loses) + '%',
                    "Bust": {
                        "Count": player.stats.get('Bust'),
                        "Percent": '{0:.2f}'.format(busts) + '%',
                    },
                },
                "Push": {
                    "Count": player.stats.get('Push'),
                    "Percent": '{0:.2f}'.format(pushes) + '%',
                },
                "Round": {
                    "Count": player.stats.get('Rounds')
                }
            },
            "Decision Stats": {
                "Hit": {
                    "Count": player.stats.get('Hit'),
                    "Percent": '{0:.2f}'.format(hits) + '%',
                },
                "Stand": {
                    "Count": player.stats.get('Stand'),
                    "Percent": '{0:.2f}'.format(stands) + '%',
                },
                "Double Down": {
                    "Count": player.stats.get('Double down'),
                    "Percent": '{0:.2f}'.format(doubles) + '%',
                },
                "Decision": {
                    "Count": player.stats.get('Decisions')
                }
            }
        }
        print(json.dumps(stats, sort_keys=False, indent=4))
        time.sleep(self.turn_time * 5)

    '''
    End the round
    '''

    def end_round(self):
        for player in self.players:
            player.stats['Rounds'] += 1
            if player.bankroll <= 0:
                Dealer.say(player.name + " has been eliminated.")
                Game.say(player.name + " has left the game.")
                self.round_stats(player)
                self.players.remove(player)
                self.random_player_join()
        self.throw_away_hands()
        self.round = self.round + 1

    '''
    Discard hands
    '''

    def throw_away_hands(self):
        # Throw away all cards in players' hand
        for player in self.players:
            for card in player.hand.cards:
                self.dealer.discard.append(card)
            player.hand.cards = []
        # Throw away all cards in dealer's hand
        for card in self.dealer.hand.cards:
            self.dealer.discard.append(card)
        self.dealer.hand.cards = []

    '''
    Initialize players and dealer
    '''

    def init_players(self):
        self.num_players = self.get_num_players()
        self.get_players()
        self.dealer = Dealer(self.deck_count, self.num_players, self.turn_time)

    '''
    Get the number of players playing the game
    '''

    def get_num_players(self):
        num_players = False
        while num_players is False:
            num_players = Game.input("Please enter the number of players:")
            if num_players.isdigit() is False:
                num_players = False
                Game.say("Invalid. You must enter the number of players for the game. (max = %s)" % self.max_players)
            elif int(num_players) > self.max_players:
                num_players = False
                Game.say("Max players is " + str(self.max_players))
        Game.say("Okay, there shall be %s players" % num_players)
        return num_players

    '''
    Get the players' type (Human / AI) and names
    '''

    def get_players(self):
        choice = ['y', 'n']
        type_set = False
        simulate = False
        while simulate is False:
            simulate = Game.input("Is this a simulated game? (y/n)").lower()
            if simulate not in choice:
                Dealer.say("Invalid, please enter y or n")
                simulate = False
        while type_set is False:
            for x in range(0, int(self.num_players)):
                human = False
                name = ""
                while human is False:
                    if simulate == 'y':
                        self.turn_time = 0  # speed things up
                        human = 'n'
                    else:
                        human = Game.input("Is this a human player? (y/n)").lower()
                    if human in choice:
                        player = None
                        if human == 'y':
                            name = Game.get_player_name('Human')
                            player = Player(name)
                        elif human == 'n':
                            # name = Game.get_player_name('AI')
                            name = names.get_full_name()
                            player = Bot(name)
                        # Add to players array
                        Dealer.say("Hello " + name + ". Please take your seat.")
                        self.players.append(player)
                    else:
                        Dealer.say("Invalid, please enter y or n")
                        human = False
            # Exit loop
            type_set = True

    '''
    A 25% chance a new player joins the table
    '''

    def random_player_join(self):
        chance = randint(1, 4)
        if chance == 1:
            name = names.get_full_name()
            player = Bot(name)
            self.players.append(player)
            Game.say("A new player has joined the table.")
            Dealer.say("Hello " + name + ". Please take your seat.")
            self.dealer.bank += self.dealer.player_multiplier

    '''
    Get the player's name
    '''

    @staticmethod
    def get_player_name(player_type):
        return Game.input("What is the " + player_type + "'s name?")

    '''
    Take input
    '''

    @staticmethod
    def input(message):
        pit_boss = colored("\nPit Boss: ", 'green')
        colored_msg = colored(message, 'white')
        return input(pit_boss + colored_msg + ' ')

    '''
    Say something
    '''

    @staticmethod
    def say(message):
        pit_boss = colored("\nPit Boss: ", 'green')
        colored_msg = colored(message, 'white')
        print(pit_boss + colored_msg)

    '''
    Generic message
    '''

    @staticmethod
    def msg(message):
        colored_msg = colored(message, 'white')
        print("\n" + colored_msg)

# END
