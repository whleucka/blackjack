from random import randint

from termcolor import colored

from player import Player


class Bot(Player):
    def __init__(self, name):
        Player.__init__(self, name)

    '''
    Make a wager (AI)
    '''

    def make_wager(self):
        choice = randint(0, 100)
        max_range = 1000
        conservative = randint(100, max(round(self.bankroll * 0.01), max_range))
        fair = randint(100, max(round(self.bankroll * 0.025), max_range * 2))
        greedy = randint(1000, max(round(self.bankroll * 0.05), max_range * 4))
        extreme = randint(2000, max(round(self.bankroll * 0.1), max_range * 6))

        bet_strategy = fair
        if 50 <= choice < 80:
            bet_strategy = conservative
        elif 80 <= choice < 90:
            bet_strategy = greedy
        elif choice >= 90:
            bet_strategy = extreme

        if bet_strategy > self.bankroll:
            bet_strategy = self.bankroll

        self.wager = bet_strategy
        player = colored("\n" + self.name + ': ', 'magenta')
        colored_msg = colored('$' + str(self.wager))
        print(player + colored_msg)

    '''
    Player decision (AI)
    '''

    def player_decision(self, dealers_card):
        move = self.recommend_move(dealers_card)
        player = colored("\n" + self.name + ': ', 'magenta')
        colored_msg = colored(self.move_translate.get(move) + ' please.')
        print(player + colored_msg)
        return move

# END
