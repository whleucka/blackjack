'''
    h: hit
    s: stand
    dh: double if allowed, otherwise hit
    ds double if allowed, otherwise stand
    p: split
    ph: split if double after split is allowed, otherwise hit
    rh: surrender if allowed, otherwise hit
'''


class Strategy:
    hard_strategy = {
        4: ['h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h'],
        5: ['h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h'],
        6: ['h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h'],
        7: ['h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h'],
        8: ['h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h'],
        9: ['h', 'dh', 'dh', 'dh', 'dh', 'h', 'h', 'h', 'h', 'h'],
        10: ['dh', 'dh', 'dh', 'dh', 'dh', 'dh', 'dh', 'dh', 'h', 'h'],
        11: ['dh', 'dh', 'dh', 'dh', 'dh', 'dh', 'dh', 'dh', 'dh', 'h'],
        12: ['h', 'h', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'],
        13: ['s', 's', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'],
        14: ['s', 's', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'],
        15: ['s', 's', 's', 's', 's', 'h', 'h', 'h', 'rh', 'h'],
        16: ['s', 's', 's', 's', 's', 'h', 'h', 'rh', 'rh', 'rh'],
        17: ['s', 's', 's', 's', 's', 's', 's', 's', 's', 's'],
        18: ['s', 's', 's', 's', 's', 's', 's', 's', 's', 's'],
        19: ['s', 's', 's', 's', 's', 's', 's', 's', 's', 's'],
        20: ['s', 's', 's', 's', 's', 's', 's', 's', 's', 's'],
        21: ['s', 's', 's', 's', 's', 's', 's', 's', 's', 's']
    }

    soft_strategy = {
        13: ['h', 'h', 'h', 'dh', 'dh', 'h', 'h', 'h', 'h', 'h'],
        14: ['h', 'h', 'h', 'dh', 'dh', 'h', 'h', 'h', 'h', 'h'],
        15: ['h', 'h', 'dh', 'dh', 'dh', 'h', 'h', 'h', 'h', 'h'],
        16: ['h', 'h', 'dh', 'dh', 'dh', 'h', 'h', 'h', 'h', 'h'],
        17: ['h', 'dh', 'dh', 'dh', 'dh', 'h', 'h', 'h', 'h', 'h'],
        18: ['s', 'ds', 'ds', 'ds', 'ds', 's', 's', 'h', 'h', 'h'],
        19: ['s', 's', 's', 's', 's', 's', 's', 's', 's', 's'],
        20: ['s', 's', 's', 's', 's', 's', 's', 's', 's', 's'],
        21: ['s', 's', 's', 's', 's', 's', 's', 's', 's', 's'],
    }

    split_strategy = {
        "22": ['ph', 'ph', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h'],
        "33": ['ph', 'ph', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h'],
        "44": ['h', 'h', 'h', 'ph', 'ph', 'ph', 'h', 'h', 'h', 'h'],
        "66": ['ph', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h', 'h'],
        "77": ['p', 'p', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h'],
        "88": ['p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p'],
        "99": ['p', 'p', 'p', 'p', 'p', 's', 'p', 'p', 's', 's'],
        "AA": ['p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p'],
    }

    def __init__(self):
        pass

# END
