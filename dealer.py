import time

from termcolor import colored

from deck import Deck
from hand import Hand


class Dealer:
    bank = None
    player_multiplier = 35000
    shoe = []
    num_decks = None
    num_cards = None
    discard = []
    hand = None
    shuffle_threshold = 20
    turn_time = None

    def __init__(self, num_decks, num_players, turn_time):
        self.hand = Hand()
        self.num_decks = num_decks
        self.num_cards = 52 * num_decks
        self.bank = self.player_multiplier * int(num_players)
        self.turn_time = turn_time
        self.load_shoe()

    '''
    Initialize decks, shuffle, load shoe
    '''

    def load_shoe(self):
        deck = None
        for x in range(0, self.num_decks):
            deck = Deck()
            deck.shuffle()
        for card in deck.cards:
            self.shoe.append(card)
        time.sleep(self.turn_time * 5)

    '''
    Take input
    '''

    @staticmethod
    def input(message):
        dealer = colored("\nDealer: ", 'blue')
        colored_msg = colored(message, 'white')
        return input(dealer + colored_msg + ' ')

    '''
    Say something
    '''

    @staticmethod
    def say(message):
        dealer = colored("\nDealer: ", 'blue')
        colored_msg = colored(message, 'white')
        print(dealer + colored_msg)

    '''
    How many cards in the shoe remaining
    '''

    def cards_left(self):
        return len(self.shoe)

    '''
    Draw a card from the shoe
    '''

    def draw_card(self):
        if self.cards_left() <= self.shuffle_threshold:
            Dealer.say("Shuffle up!!!")
            self.discard = []
            self.load_shoe()
        # Burn the first card
        burn_card = self.shoe.pop()
        self.discard.append(burn_card)
        return self.shoe.pop()

    '''
    Make decision for house
    '''

    def make_decision(self):
        hand_value = self.hand.get_value()
        play = ''
        if hand_value >= 17:
            play = 's'
        else:
            play = 'h'
        return play

    '''
    Displays shoe
    '''

    def peek_shoe(self):
        for card in self.shoe:
            print(card.get_card())

    '''
    Display current hand
    '''

    def peek_hand(self):
        self.hand.get_hand()

    '''
    Displays current discard pile
    '''

    def peek_discard(self):
        for card in self.discard:
            print(card.get_card())

# END
