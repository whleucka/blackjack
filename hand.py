class Hand:
    cards = None

    def __init__(self):
        self.cards = []

    '''
    Print current hand state
    '''
    def get_hand(self):
        hand = ""
        for card in self.cards:
            hand += card.get_card() + ' '
        print(hand)

    '''
    Hand contains one or more ace's
    '''

    def has_ace(self):
        for card in self.cards:
            if card.get_pip() == 'A':
                return True
        return False

    '''
    Returns a string of pips eg, AA, 2A, K10
    Used for split_strategy
    '''

    def get_pips(self):
        pips = ""
        for card in self.cards:
            pips += card.get_pip()
        return pips

    '''
    Get the hard value of the hand
    (Aces are counted as 1 or 11)
    '''

    def get_value(self):
        value = 0
        aces = []
        # Loop around cards in the hand, extract aces because
        # they are special. They can either be 1 or 11
        for card in self.cards:
            if card.get_pip() == 'A':
                aces.append(card)
            else:
                value += card.get_value()

        if len(aces) == 0:
            # No aces, just sum the value and return
            return value
        else:
            for ace in aces:
                if value + 11 < 22:
                    value += 11
                else:
                    value += 1
            return value

    '''
    Get the soft value of the hand 
    (Aces are counted as 1)
    '''

    def get_value_soft(self):
        value = 0
        for card in self.cards:
            if card.get_pip() == 'A':
                value += 1
            else:
                value += card.get_value()

        return value
# END
