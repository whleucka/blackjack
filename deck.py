import random

from card import Card


class Deck:
    cards = []

    def __init__(self):
        self.init_deck()

    '''
    Initialize the deck
    '''

    def init_deck(self):
        for suit in range(0, 4):
            for pip in range(0, 13):
                self.cards.append(Card(suit, pip))

    '''
    Shuffle the deck
    '''

    def shuffle(self):
        random.shuffle(self.cards)

# END
