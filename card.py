from termcolor import colored


class Card:
    suit = None
    pip = None

    suitMap = {
        0: u"\u2665",  # HEARTS
        1: u"\u2663",  # CLUBS
        2: u"\u2666",  # DIAMONDS
        3: u"\u2660"  # SPADES
    }

    pipMap = {
        0: "2",
        1: "3",
        2: "4",
        3: "5",
        4: "6",
        5: "7",
        6: "8",
        7: "9",
        8: "10",
        9: "J",
        10: "Q",
        11: "K",
        12: "A"
    }

    valueMap = {
        0: 2,
        1: 3,
        2: 4,
        3: 5,
        4: 6,
        5: 7,
        6: 8,
        7: 9,
        8: 10,
        9: 10,
        10: 10,
        11: 10,
        12: 11
    }

    cardSuitMap = {
        0: "B",  # HEARTS
        1: "D",  # CLUBS
        2: "C",  # DIAMONDS
        3: "A"  # SPADES
    }

    cardPipMap = {
        0: "2",
        1: "3",
        2: "4",
        3: "5",
        4: "6",
        5: "7",
        6: "8",
        7: "9",
        8: "A",
        9: "B",
        10: "D",
        11: "E",
        12: "1"
    }

    def __init__(self, suit, pip):
        self.suit = suit
        self.pip = pip

    '''
    Return's a unicode a card
    '''

    def get_card(self):
        card = chr(int('0001f0%s%s' % (
            self.get_card_suit(),
            self.get_card_pip()
        ), base=16))
        # Suit is Heart / Diamond
        red = [0, 2]
        if self.suit in red:
            red_card = colored(card, 'red')
            return red_card
        else:
            black_card = colored(card, 'white')
            return black_card

    '''
    Get the suit
    '''

    def get_card_suit(self):
        return self.cardSuitMap.get(self.suit)

    '''
    Get the pip
    '''

    def get_card_pip(self):
        return self.cardPipMap.get(self.pip)

    '''
    Get the suit
    '''

    def get_suit(self):
        return self.suitMap.get(self.suit)

    '''
    Get the pip
    '''

    def get_pip(self):
        return self.pipMap.get(self.pip)

    '''
    Value of the card
    '''

    def get_value(self):
        return self.valueMap.get(self.pip)

# END
